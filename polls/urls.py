from django.urls import path

from . import views 
 

# django documentacion parte 4


app_name = 'polls'
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"), 
    path(" ", views.DetailView.as_view(), name="detail"), # esto no esta funcionando 
    path("<int:pk>/results/", views.ResultsView.as_view(), name="results"), # esto no esta funcionando
    path("<int:question_id>/vote/", views.vote, name="vote"), 
]
    
    
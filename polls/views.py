from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic


from .models import Choice, Question



# Aqui agregamos el primer paso de la parte 4 de la documentacion dajngo; implantando nuevas vistas

class IndexView(generic.ListView):
    template_name = "polls/index.html" # esto lo que hace es mostararme mi platilla html de index
    context_object_name = "latest_question_list" #  esto pasa la informacion de la encuesta

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by("pub_date")[:7]   # muestra los 7 ultimas preguntas de la encuesta


class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/detail.html"


class ResultsView(generic.DetailView):
    model = Question
    template_name = "polls/results.html"
    
    


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST["choice"])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(
            request,
            "polls/detail.html",
            {
                "question": question,
                "error_message": " Selecciona una opcion .",
            },
        )
    else:
        selected_choice.votes = F("votes") + 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse("polls:results", args=(question.id,)))